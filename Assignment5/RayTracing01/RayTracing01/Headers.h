#pragma once

#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>		// All the operating system calls (create window, etc) - Win32 library
#include <stdio.h>			// Old fashioned C IO - allows us to read files, etc

#include <d3d12.h>			// DirectX 12
#include <dxgi1_6.h>		// General graphics facilities (don't vary so much with version of DX)
#include <dxgidebug.h>
#include <D3Dcompiler.h>	// The shader compiler
#include <DirectXMath.h>	// A maths library with matrices and vectors etc

