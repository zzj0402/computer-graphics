#include "Headers.h"
#include "BitmapReader.h"
#include "ObjReader.h"

#include "CompiledShaders\Raytracing.hlsl.h"

using namespace DirectX;

// Settings
const int WIDTH = 800;
const int HEIGHT = 800;
const int BUFFER_COUNT = 2;
const bool DEBUG_INTERFACE_ENABLED = true;

// Ray tracing program information
struct RTVertex { XMFLOAT3 position; };

struct Vertex { XMFLOAT3 position, normal; XMFLOAT2 uv; };
const UINT vertexSize = sizeof(Vertex);
UINT vertexCount;

struct Viewport { float topleftx, toplefty, topleftz, bottomrightx, bottomrighty, bottomrightz; };
struct RayGenConstantBuffer { Viewport viewport; };
// Initialize raygen constant buffer (TODO - allow for aspect ratio and change on window resizing)
RayGenConstantBuffer raygenConstantBufferData; // = { { { -5.0f, -5.0f, 0.0f }, {5.0f, 5.0f, 0.0f } } };
const UINT rayGenConstSize32 = (sizeof(RayGenConstantBuffer) - 1) / 4 + 1;

struct ConstantBuffer { XMVECTOR cameraPosition, forward, right, up; float seconds; };
ConstantBuffer constantBufferData;

// State
HWND MainWindowHandle;
int backBufferIndex;
UINT64 bufferFenceValue[BUFFER_COUNT] = { 0, 0 };
bool keyMap[256] = {};  // Set when key is down indexed by VK code
float heading = 0.0f, elevation = 0.0f;
XMVECTOR cameraPosition = { 0.0f, 0.0f, 7.0f };

///////////////////////////////////////////////////////////////////
// DirectX Resources needing global access in order of creation  //
///////////////////////////////////////////////////////////////////

// DirectX 12 pipeline resources (display buffers and structure)
ID3D12Debug1 *debugController = NULL;
IDXGIInfoQueue *debugInfoQueue = NULL;  // New for ray tracing
IDXGIFactory4 *factory = NULL;
IDXGIAdapter1 *adapter = NULL;
ID3D12Device5 *device = NULL;  // Version 5 used for ray tracing
ID3D12CommandQueue *commandQueue = NULL;
IDXGISwapChain3 *swapChain = NULL;
ID3D12DescriptorHeap *rtvHeap = NULL;
unsigned int rtvDescriptorSize;
ID3D12DescriptorHeap *srvHeap = NULL;
unsigned int srvDescriptorSize;
ID3D12DescriptorHeap *dsvHeap = NULL;
ID3D12DescriptorHeap *rtHeap = NULL;  // New for ray tracing
unsigned int rtDescriptorSize; // New for ray tracing
ID3D12Resource *renderTargets[BUFFER_COUNT] = { NULL, NULL };
ID3D12CommandAllocator *directCommandAllocator[BUFFER_COUNT] = { NULL, NULL };
ID3D12CommandAllocator *bundleCommandAllocator = NULL;
ID3D12Resource *depthBuffer = NULL;

ID3D12GraphicsCommandList4 *commandList = NULL;

// Ray tracing
ID3D12RootSignature *globalRTRootSignature = NULL;
ID3D12RootSignature *localRTRootSignature = NULL;
ID3D12StateObject *rtStateObject = NULL;
ID3D12Resource *vertexBuffer = NULL;
//ID3D12Resource *rtVertexBuffer = NULL;
//ID3D12Resource *rtIndexBuffer = NULL;
ID3D12Resource *constantBuffer = NULL;
UINT8 *constantBufferMappedPointer;
UINT8 *rtConstantBufferMappedPointer;
ID3D12Resource *texture = NULL;
ID3D12Resource *textureUpload = NULL;
ID3D12Resource *scratchArea = NULL;
ID3D12Resource *topLevelAccStr = NULL;
ID3D12Resource *bottomLevelAccStr = NULL;
ID3D12Resource *instanceDescription = NULL;
ID3D12Resource *raygenShaderTable = NULL;
ID3D12Resource *missShaderTable = NULL;
ID3D12Resource *hitgroupShaderTable = NULL;
ID3D12Resource *rtOutputBuffer = NULL;

// Prerecorded commands
ID3D12GraphicsCommandList *bundle;

// Synchronisation primitives
ID3D12Fence *fence = NULL;
HANDLE fenceEvent = NULL;

void ErrorExit()
{
	printf("Press <Enter> to exit\n");
	getchar();
	exit(-1);
}

template<class T>void FreeResource(T *&resource, const char *name, bool checkCount)
{
	if (resource != NULL) {
		int count = resource->Release();
		if (checkCount && count != 0) {
			printf("Resource count not zero(%d) on release for %s\n", count, name);
			ErrorExit();
		}
		resource = NULL;
	}
}

void FreeResources()
{
	// Synchronisation primitives
	if (fenceEvent != NULL) {
		CloseHandle(fenceEvent);
		fenceEvent = NULL;
	}
	FreeResource(fence, "Fence", true);

	// Assets
	// Ray tracing items
	FreeResource(rtOutputBuffer, "Ray Tracing Output Buffer", true);
	FreeResource(hitgroupShaderTable, "Hit Group Shader Table", true);
	if (raygenShaderTable != NULL) raygenShaderTable->Unmap(0, NULL);
	FreeResource(missShaderTable, "Miss Shader Table", true);
	FreeResource(raygenShaderTable, "Ray Generation Shader Table", true);
	FreeResource(instanceDescription, "Ray Tracing Acceleration Structure Instance Description", true);
	FreeResource(bottomLevelAccStr, "Ray Tracing Bottom Level Acceleration Structure", true);
	FreeResource(topLevelAccStr, "Ray Tracing Top Level Acceleration Structure", true);
	FreeResource(scratchArea, "Ray Tracing Acceleration Structure Construction Area", true);
	//FreeResource(rtIndexBuffer, "Ray Tracing Index Buffer", true);
	//FreeResource(rtVertexBuffer, "Ray Tracing Vertex Buffer", true);
	FreeResource(textureUpload, "Texture Upload Buffer", true);
	FreeResource(texture, "Texture", true);
	if (constantBuffer != NULL) constantBuffer->Unmap(0, NULL);
	FreeResource(constantBuffer, "Constant Buffer", true);
	FreeResource(vertexBuffer, "Vertex Buffer", true);
	FreeResource(rtStateObject, "Raytracing State Object(hw)", true);
	FreeResource(localRTRootSignature, "Local Raytracing Root Signature", true);
	FreeResource(globalRTRootSignature, "Global Raytracing Root Signature", true);

	// Command list
	FreeResource(commandList, "Command List", true);

	// Pipeline
	FreeResource(depthBuffer, "Depth Stencil Buffer", true);
	FreeResource(bundleCommandAllocator, "Bundle Command Allocator", true);
	for (int f = 0; f < BUFFER_COUNT; f++) {
		FreeResource(directCommandAllocator[f], "Command Allocator", true);
		FreeResource(renderTargets[f], "Render Target", f == BUFFER_COUNT - 1);
	}
	
	FreeResource(rtHeap, "Raytracing Shader Resource Heap", true);  // Ray tracing
	
	FreeResource(dsvHeap, "Depth Stencil View Heap", true);
	FreeResource(srvHeap, "Shader Resource Descriptor Heap", true);
	FreeResource(rtvHeap, "Render Target Descriptor Heap", true);
	FreeResource(swapChain, "Swap Chain", true);
	FreeResource(commandQueue, "Command Queue", true);
	FreeResource(device, "Graphics Device", true);
	FreeResource(adapter, "Graphics Adapter", true);
	FreeResource(factory, "DXGI Factory", true);
	FreeResource(debugInfoQueue, "Debug Info Queue", false);  // ref count not zero?
	FreeResource(debugController, "Debug Interface", true);
}

void FreeAndExit(const char* message)
{
	printf("Error: %s\n", message);
	FreeResources();
	ErrorExit();
}

UINT RoundUp(UINT x, UINT unit)
{
	return (x + unit - 1) & (~(unit - 1));
}

void WaitForCommandCompletion(UINT64 fenceValue)
{
	if (commandQueue->Signal(fence, fenceValue) != S_OK)
		FreeAndExit("Couldn't set signal on fence in command queue");

	if (fence->GetCompletedValue() < fenceValue) {
		if (fence->SetEventOnCompletion(fenceValue, fenceEvent) != S_OK)
			FreeAndExit("Couldn't set event on fence");
		WaitForSingleObject(fenceEvent, INFINITE);
	}
}

void CreateDirectX12Pipeline()
{
	/* Enable the D3D12 debug layer */ {
		if (DEBUG_INTERFACE_ENABLED) {
			if (D3D12GetDebugInterface(__uuidof(ID3D12Debug1), (void **)&debugController) != S_OK)
				FreeAndExit("Couldn't create debug layer");
			debugController->EnableDebugLayer();

			if (DXGIGetDebugInterface1(0, __uuidof(IDXGIInfoQueue), (void **)&debugInfoQueue) != S_OK)
				FreeAndExit("Couldn't get debug interface\n");
		}
		//debugController->SetEnableSynchronizedCommandQueueValidation(true);
		//printf("Have enabled synchronized command queue validation\n");
		//debugController->SetEnableGPUBasedValidation(true);
		//printf("Have enabled GPU based validation\n");
	}

	/* Create the device */ {
		// Begin by creating factory(4) and (RT) checking for the allow tearing flag on factory(5)
		if (CreateDXGIFactory2(DEBUG_INTERFACE_ENABLED ? DXGI_CREATE_FACTORY_DEBUG : 0, __uuidof(IDXGIFactory4), (void **)&factory) != S_OK)
			FreeAndExit("Couldn't create IDXGIFactory4");

		// New for ray tracing
		IDXGIFactory5 *factory5 = NULL;
		if (factory->QueryInterface(__uuidof(IDXGIFactory5), (void **)&factory5) != S_OK)
			FreeAndExit("Couldn't get factory 5 interface from factory 4");
		DWORD tearing;   // TODO - may need this some time (check fails if type of 'tearing' is bool)
		if (factory5->CheckFeatureSupport(DXGI_FEATURE_PRESENT_ALLOW_TEARING, &tearing, sizeof(tearing)) != S_OK)
			FreeAndExit("Couldn't do feature check on 'allow tearing'");
		//printf("Allow tearing check => %s\n", tearing ? "allowed" : "not allowed");
		factory5->Release();

		// Scan available adapters to find an NVIDIA hardware adapter
		for (unsigned int i = 0; factory->EnumAdapters1(i, &adapter) != DXGI_ERROR_NOT_FOUND; i++) {
			printf("Found adapter %d\n", i);
			DXGI_ADAPTER_DESC1 desc;
			adapter->GetDesc1(&desc);
			printf("  Description: %S\n", desc.Description);
			printf("  Kind: %s\n", desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE ? "Software" : "Hardware");
			printf("  Video memory: %llx\n", desc.DedicatedVideoMemory);
			printf("  System memory: %llx\n", desc.DedicatedSystemMemory);
			if ((desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) == 0 && wcsstr(desc.Description, L"NVIDIA") != NULL) {
				break;
			}
			FreeResource(adapter, "Graphics Adapter (unsuitable)", true);
		}
		if (adapter == NULL)
			FreeAndExit("Couldn't find an NVIDIA hardware graphics adapter");

		// Create Graphics device checking that the adapter supports DirectX 12.1 (RT fallback only needs 11.0?)
		ID3D12Device *tempDevice;
		if (D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_12_1, _uuidof(ID3D12Device), (void **)&tempDevice) != S_OK)
			FreeAndExit("NVIDIA graphics adapter doesn't support DirectX 12.1");
		// For ray tracing - obtain a more recent version of device (V5)
		if (tempDevice->QueryInterface(__uuidof(ID3D12Device5), (void **)&device) != S_OK) {
			tempDevice->Release();
			FreeAndExit("Couldn't get DirectX Raytracing interface for the device");
		}
		tempDevice->Release();
	}

	/* Describe and create the command queue */ {
		D3D12_COMMAND_QUEUE_DESC qdesc;
		qdesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
		qdesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
		qdesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
		qdesc.NodeMask = 0;
		if (device->CreateCommandQueue(&qdesc, __uuidof(ID3D12CommandQueue), (void **)&commandQueue) != S_OK)
			FreeAndExit("Couldn't create Command Queue");
	}

	/* Describe and create the swap chain */ {
		DXGI_SWAP_CHAIN_DESC1 scdesc;
		scdesc.Width = WIDTH;
		scdesc.Height = HEIGHT;
		scdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		scdesc.Stereo = FALSE;
		scdesc.SampleDesc.Count = 1;
		scdesc.SampleDesc.Quality = 0;
		scdesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scdesc.BufferCount = BUFFER_COUNT;
		scdesc.Scaling = DXGI_SCALING_STRETCH;
		scdesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		scdesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
		scdesc.Flags = 0;

		IDXGISwapChain1 *sctemp = NULL;
		if (factory->CreateSwapChainForHwnd(commandQueue, MainWindowHandle, &scdesc, NULL, NULL, &sctemp) != S_OK)
			FreeAndExit("Couldn't create Swap Chain");

		// This sample does not support fullscreen transitions.
		if (factory->MakeWindowAssociation(MainWindowHandle, DXGI_MWA_NO_ALT_ENTER) != S_OK)
			FreeAndExit("Couldn't block fullscreen transitions");

		if (sctemp->QueryInterface(__uuidof(IDXGISwapChain3), (void **)&swapChain) != S_OK)
			FreeAndExit("Couldn't get SwapChain3 interface from SwapChain1 object");

		backBufferIndex = swapChain->GetCurrentBackBufferIndex();
		sctemp->Release();
		sctemp = NULL;
	}

	/* Create descriptor heaps */ {
		// Heap for render target views
		D3D12_DESCRIPTOR_HEAP_DESC rtvhdesc;
		rtvhdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		rtvhdesc.NumDescriptors = BUFFER_COUNT;
		rtvhdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		rtvhdesc.NodeMask = 0;
		if (device->CreateDescriptorHeap(&rtvhdesc, __uuidof(ID3D12DescriptorHeap), (void **)&rtvHeap) != S_OK)
			FreeAndExit("Couldn't create render target descriptor heap");
		rtvDescriptorSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

		// Heap for shader resource views (texture and constant buffers)
		D3D12_DESCRIPTOR_HEAP_DESC srvhdesc;
		srvhdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		srvhdesc.NumDescriptors = 1;
		srvhdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		srvhdesc.NodeMask = 0;
		if (device->CreateDescriptorHeap(&srvhdesc, __uuidof(ID3D12DescriptorHeap), (void **)&srvHeap) != S_OK)
			FreeAndExit("Couldn't create shader resource view descriptor heap");
		srvDescriptorSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		// Heap for depth stencil view
		D3D12_DESCRIPTOR_HEAP_DESC dsvhdesc;
		dsvhdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
		dsvhdesc.NumDescriptors = 2;
		dsvhdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		dsvhdesc.NodeMask = 0;
		if (device->CreateDescriptorHeap(&dsvhdesc, __uuidof(ID3D12DescriptorHeap), (void **)&dsvHeap) != S_OK)
			FreeAndExit("Couldn't create depth stencil view descriptor heap");

		// Heap for ray tracing shader resources (high and low level acceleration structures and output texture)
		D3D12_DESCRIPTOR_HEAP_DESC rthdesc;
		rthdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		rthdesc.NumDescriptors = 2;
		rthdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		rthdesc.NodeMask = 0;
		if (device->CreateDescriptorHeap(&rthdesc, __uuidof(ID3D12DescriptorHeap), (void **)&rtHeap) != S_OK)
			FreeAndExit("Couldn't create ray tracing shader resource descriptor heap");
		rtDescriptorSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	}

	/* Create frame resource and command allocator for each render frame */ {
		D3D12_CPU_DESCRIPTOR_HANDLE rtvCPUDescriptor = rtvHeap->GetCPUDescriptorHandleForHeapStart();
		for (int f = 0; f < BUFFER_COUNT; f++) {
			if (swapChain->GetBuffer(f, __uuidof(ID3D12Resource), (void **)&renderTargets[f]) != S_OK)
				FreeAndExit("Couldn't get swap chain buffer");
			device->CreateRenderTargetView(renderTargets[f], NULL, rtvCPUDescriptor);
			rtvCPUDescriptor.ptr += rtvDescriptorSize;
			if (device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, __uuidof(ID3D12CommandAllocator), (void **)&directCommandAllocator[f]) != S_OK)
				FreeAndExit("Couldn't create direct command allocator");
		}
		if (device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_BUNDLE, __uuidof(ID3D12CommandAllocator), (void **)&bundleCommandAllocator) != S_OK)
			FreeAndExit("Couldn't create bundle command allocator");
	}

	/* Create depth stencil buffer and descriptor */ {
		D3D12_HEAP_PROPERTIES dhprop;
		dhprop.Type = D3D12_HEAP_TYPE_DEFAULT;
		dhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		dhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		dhprop.CreationNodeMask = 0;
		dhprop.VisibleNodeMask = 0;

		D3D12_RESOURCE_DESC ddesc;
		ddesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
		ddesc.Alignment = 0;
		ddesc.Width = WIDTH;
		ddesc.Height = HEIGHT;
		ddesc.DepthOrArraySize = 1;
		ddesc.MipLevels = 1;
		ddesc.Format = DXGI_FORMAT_D32_FLOAT;
		ddesc.SampleDesc.Count = 1;
		ddesc.SampleDesc.Quality = 0;
		ddesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
		ddesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

		if (device->CreateCommittedResource(&dhprop, D3D12_HEAP_FLAG_NONE, &ddesc, D3D12_RESOURCE_STATE_DEPTH_WRITE, NULL, __uuidof(ID3D12Resource), (void **)&depthBuffer) != S_OK) {
			FreeAndExit("Couldn't depth buffer");
		}

		D3D12_DEPTH_STENCIL_VIEW_DESC dsvdesc;
		dsvdesc.Format = ddesc.Format;
		dsvdesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
		dsvdesc.Flags = D3D12_DSV_FLAG_NONE;
		dsvdesc.Texture2D.MipSlice = 0;

		device->CreateDepthStencilView(depthBuffer, &dsvdesc, dsvHeap->GetCPUDescriptorHandleForHeapStart());
	}
}

void CreateVertexBuffer(const char *filename, ID3D12Resource *&vertexBuffer, UINT &vertexCount)
{
	/* Create the vertex buffer, transfer data and make buffer view(description?) */ {
		FILE *modelFile;
		UINT sizeReqdFloats;
		UINT reqdChunkSpace;

		if (fopen_s(&modelFile, "cube.obj", "r") != 0)
			FreeAndExit("Couldn't open model file");

		if (!OFR1111CheckAndCountParser(modelFile, sizeReqdFloats, reqdChunkSpace)) {
			fclose(modelFile);
			FreeAndExit("Parsing error in obj file\n");
		}
		int vertexBufferSize = sizeReqdFloats * sizeof(float);
		vertexCount = sizeReqdFloats / 8;

		D3D12_HEAP_PROPERTIES vbhprop;
		vbhprop.Type = D3D12_HEAP_TYPE_UPLOAD;
		vbhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		vbhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		vbhprop.CreationNodeMask = 0;
		vbhprop.VisibleNodeMask = 0;

		D3D12_RESOURCE_DESC vbdesc;
		vbdesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
		vbdesc.Alignment = 0;
		vbdesc.Width = vertexBufferSize;
		vbdesc.Height = 1;
		vbdesc.DepthOrArraySize = 1;
		vbdesc.MipLevels = 1;
		vbdesc.Format = DXGI_FORMAT_UNKNOWN;
		vbdesc.SampleDesc.Count = 1;
		vbdesc.SampleDesc.Quality = 0;
		vbdesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		vbdesc.Flags = D3D12_RESOURCE_FLAG_NONE;

		// Note - this is not the preferred method of filling a vertex buffer that will
		// be used more than once - should upload and transfer to a default heap area
		if (device->CreateCommittedResource(
			&vbhprop, D3D12_HEAP_FLAG_NONE, &vbdesc,
			D3D12_RESOURCE_STATE_GENERIC_READ, NULL,
			__uuidof(ID3D12Resource), (void **)&vertexBuffer) != S_OK) {
			fclose(modelFile);
			FreeAndExit("Couldn't create vertex buffer");
		}

		UINT8 *vertexBufferPtr;
		D3D12_RANGE readRange;
		readRange.Begin = 0;
		readRange.End = 0;
		if (vertexBuffer->Map(0, &readRange, (void **)&vertexBufferPtr) != S_OK) {
			fclose(modelFile);
			FreeAndExit("Couldn't map vertex buffer");
		}
		OFR2222CollectAndFillParser(modelFile, reqdChunkSpace, (float *)vertexBufferPtr);
		fclose(modelFile);
		vertexBuffer->Unmap(0, NULL);

		// Vertex buffer is directly bound (GPU address only, no view) in the command list
	}
}

void CreateConstantBuffer() {
	D3D12_HEAP_PROPERTIES cbhprop;
	cbhprop.Type = D3D12_HEAP_TYPE_UPLOAD;
	cbhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	cbhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	cbhprop.CreationNodeMask = 0;
	cbhprop.VisibleNodeMask = 0;

	D3D12_RESOURCE_DESC cbdesc;
	cbdesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	cbdesc.Alignment = 0;
	cbdesc.Width = 1024 * 64;
	cbdesc.Height = 1;
	cbdesc.DepthOrArraySize = 1;
	cbdesc.MipLevels = 1;
	cbdesc.Format = DXGI_FORMAT_UNKNOWN;
	cbdesc.SampleDesc.Count = 1;
	cbdesc.SampleDesc.Quality = 0;
	cbdesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	cbdesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	if (device->CreateCommittedResource(&cbhprop, D3D12_HEAP_FLAG_NONE, &cbdesc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL, __uuidof(ID3D12Resource), (void **)&constantBuffer) != S_OK)
		FreeAndExit("Couldn't create constant(upload) buffer");

	// Map and initialise the constant buffer
	D3D12_RANGE readRange;
	readRange.Begin = 0;
	readRange.End = 0;
	if (constantBuffer->Map(0, &readRange, (void **)&constantBufferMappedPointer) != S_OK)
		FreeAndExit("Couldn't map constant buffer");
	memcpy(constantBufferMappedPointer, &constantBufferData, sizeof(ConstantBuffer));
	// Leave constant buffer mapped for updating on each frame
}

void CreateTexture() {
	// Start reading the texture file, to obtain width and height
	int textureWidth, textureHeight;
	if (!LoadBMPFile("TexturesCom_Grass0100_1_seamless_S.bmp", 1, &textureWidth, &textureHeight, NULL, 0, 0, 0))
		FreeAndExit("Couldn't load texture file");

	// Create the texture buffer
	D3D12_HEAP_PROPERTIES texhprop;
	texhprop.Type = D3D12_HEAP_TYPE_DEFAULT;
	texhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	texhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	texhprop.CreationNodeMask = 0;
	texhprop.VisibleNodeMask = 0;

	D3D12_RESOURCE_DESC texdesc;  // texture resource description
	texdesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	texdesc.Alignment = 0;
	texdesc.Width = textureWidth;
	texdesc.Height = textureHeight;
	texdesc.DepthOrArraySize = 1;
	texdesc.MipLevels = 1;
	texdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texdesc.SampleDesc.Count = 1;
	texdesc.SampleDesc.Quality = 0;
	texdesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;  // TODO ???? should this be row major
	texdesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	if (device->CreateCommittedResource(&texhprop, D3D12_HEAP_FLAG_NONE, &texdesc, D3D12_RESOURCE_STATE_COPY_DEST, NULL, __uuidof(ID3D12Resource), (void **)&texture) != S_OK) {
		LoadBMPFile("", 3, NULL, NULL, NULL, 0, 0, 0);
		FreeAndExit("Couldn't create buffer resource for texture");
	}

	// Determine size and layout for texture upload buffer
	D3D12_PLACED_SUBRESOURCE_FOOTPRINT footprint;
	UINT rowCount;
	UINT64 rowLength;
	UINT64 uploadReqdSize = 0;
	device->GetCopyableFootprints(&texdesc, 0, 1, 0, &footprint, &rowCount, &rowLength, &uploadReqdSize);

	// Create texture upload buffer
	D3D12_HEAP_PROPERTIES upprop;
	upprop.Type = D3D12_HEAP_TYPE_UPLOAD;
	upprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	upprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	upprop.CreationNodeMask = 0;
	upprop.VisibleNodeMask = 0;

	D3D12_RESOURCE_DESC updesc;
	updesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	updesc.Alignment = 0;
	updesc.Width = uploadReqdSize;
	updesc.Height = 1;
	updesc.DepthOrArraySize = 1;
	updesc.MipLevels = 1;
	updesc.Format = DXGI_FORMAT_UNKNOWN;
	updesc.SampleDesc.Count = 1;
	updesc.SampleDesc.Quality = 0;
	updesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	updesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	if (device->CreateCommittedResource(&upprop, D3D12_HEAP_FLAG_NONE, &updesc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL, __uuidof(ID3D12Resource), (void **)&textureUpload) != S_OK) {
		LoadBMPFile("", 3, NULL, NULL, NULL, 0, 0, 0);  // just close the file
		FreeAndExit("Couldn't create texture upload buffer");
	}

	// Map upload buffer and read texture data directly into it
	UINT8 *uploadPtr;
	D3D12_RANGE readRange;
	readRange.Begin = 0;
	readRange.End = 0;
	if (textureUpload->Map(0, &readRange, (void **)&uploadPtr) != S_OK) {
		LoadBMPFile("", 3, NULL, NULL, NULL, 0, 0, 0);
		FreeAndExit("Couldn't map texture upload buffer");
	}
	if (!LoadBMPFile("", 2, NULL, NULL, uploadPtr + footprint.Offset, rowCount, (UINT)rowLength, footprint.Footprint.RowPitch)) {
		textureUpload->Unmap(0, NULL);
		FreeAndExit("Couldn't read texture data from file");
	}
	textureUpload->Unmap(0, NULL);  // TODO see if a small write range will work

	// Schedule a copy from texture upload buffer to texture buffer
	D3D12_TEXTURE_COPY_LOCATION srcLoc;
	srcLoc.pResource = textureUpload;
	srcLoc.Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
	srcLoc.PlacedFootprint = footprint;

	D3D12_TEXTURE_COPY_LOCATION destLoc;
	destLoc.pResource = texture;
	destLoc.Type = D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
	destLoc.SubresourceIndex = 0;

	commandList->CopyTextureRegion(&destLoc, 0, 0, 0, &srcLoc, NULL);

	// Add resource barrier to transition texture to pixel resource
	D3D12_RESOURCE_BARRIER barrier;
	barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	barrier.Transition.pResource = texture;
	barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
	commandList->ResourceBarrier(1, &barrier);

	// Create a Shader Resource View for the texture
	D3D12_SHADER_RESOURCE_VIEW_DESC srvdesc;
	srvdesc.Format = texdesc.Format;
	srvdesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvdesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;  // Swizzle colours
	srvdesc.Texture2D.MostDetailedMip = 0;
	srvdesc.Texture2D.MipLevels = 1;
	srvdesc.Texture2D.PlaneSlice = 0;
	srvdesc.Texture2D.ResourceMinLODClamp = 0.0f;
	D3D12_CPU_DESCRIPTOR_HANDLE rtdesch = rtHeap->GetCPUDescriptorHandleForHeapStart();
	rtdesch.ptr += rtDescriptorSize;
	device->CreateShaderResourceView(texture, &srvdesc, rtdesch);
	//device->CreateShaderResourceView(texture, &srvdesc, srvHeap->GetCPUDescriptorHandleForHeapStart());
}

void CreateAssetsRayTracing()
{
	/* Create ray tracing root signatures */ {

		/* Global Root Signature shared across all raytracing shaders invoked during a DispatchRays() call */ {	
			D3D12_DESCRIPTOR_RANGE range;
			range.RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_UAV;
			range.NumDescriptors = 1;
			range.BaseShaderRegister = 0;
			range.RegisterSpace = 0;
			range.OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

			D3D12_DESCRIPTOR_RANGE rangetex;
			rangetex.RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
			rangetex.NumDescriptors = 1;
			rangetex.BaseShaderRegister = 2;
			rangetex.RegisterSpace = 0;
			rangetex.OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

			D3D12_ROOT_PARAMETER params[4];
			params[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
			params[0].DescriptorTable.NumDescriptorRanges = 1;
			params[0].DescriptorTable.pDescriptorRanges = &range;
			params[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

			params[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
			params[1].Descriptor.ShaderRegister = 0;
			params[1].Descriptor.RegisterSpace = 0;
			params[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

			params[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
			params[2].Descriptor.ShaderRegister = 1;
			params[2].Descriptor.RegisterSpace = 0;
			params[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

			params[3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
			params[3].DescriptorTable.NumDescriptorRanges = 1;
			params[3].DescriptorTable.pDescriptorRanges = &rangetex;
			params[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

			//D3D12_STATIC_SAMPLER_DESC samplers[1];
			//samplers[0].Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
			//samplers[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			//samplers[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			//samplers[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
			//samplers[0].MipLODBias = 0.0f;
			//samplers[0].MaxAnisotropy = 0;
			//samplers[0].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
			//samplers[0].BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK;
			//samplers[0].MinLOD = 0.0f;
			//samplers[0].MaxLOD = D3D12_FLOAT32_MAX;
			//samplers[0].ShaderRegister = 0;
			//samplers[0].RegisterSpace = 0;
			//samplers[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

			D3D12_ROOT_SIGNATURE_DESC grsdesc;
			grsdesc.NumParameters = 4;
			grsdesc.pParameters = params;
			grsdesc.NumStaticSamplers = 0;
			grsdesc.pStaticSamplers = NULL; // samplers;
			grsdesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_NONE;

			ID3DBlob *sigBlob = NULL, *errBlob = NULL;
			if (D3D12SerializeRootSignature(&grsdesc, D3D_ROOT_SIGNATURE_VERSION_1, &sigBlob, &errBlob) != S_OK) {
				if (errBlob != NULL) {
					printf("Error: Failed to serialize global raytracing root signature(hw)\n");
					printf("Message: %s", (char *)errBlob->GetBufferPointer());
					errBlob->Release();
				}
				if (sigBlob != NULL) sigBlob->Release();
				FreeAndExit("Couldn't serialize global raytracing root signature(hw)");
			}
			if (device->CreateRootSignature(0, sigBlob->GetBufferPointer(), sigBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void **)&globalRTRootSignature) != S_OK) {
				sigBlob->Release();
				FreeAndExit("Couldn't create global rt root signature(hw)");
			}
			if (errBlob != NULL) errBlob->Release();
			sigBlob->Release();
		}

		/* Local Root Signatures enable shader unique arguments coming from shader tables */ {
			D3D12_ROOT_PARAMETER params[2];
			params[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
			params[0].Descriptor.ShaderRegister = 0;
			params[0].Descriptor.RegisterSpace = 0;
			params[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

			params[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
			params[1].Descriptor.ShaderRegister = 1;
			params[1].Descriptor.RegisterSpace = 0;
			params[1].Constants.Num32BitValues = rayGenConstSize32;
			params[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

			D3D12_ROOT_SIGNATURE_DESC lrsdesc;
			lrsdesc.NumParameters = 2;
			lrsdesc.pParameters = params;
			lrsdesc.NumStaticSamplers = 0;
			lrsdesc.pStaticSamplers = NULL;
			lrsdesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_LOCAL_ROOT_SIGNATURE;

			ID3DBlob *sigBlob = NULL, *errBlob = NULL;
			if (D3D12SerializeRootSignature(&lrsdesc, D3D_ROOT_SIGNATURE_VERSION_1, &sigBlob, &errBlob) != S_OK) {
				if (errBlob != NULL) {
					printf("Error: Failed to serialize local raytracing root signature(hw)\n");
					printf("Message: %s", (char *)errBlob->GetBufferPointer());
					errBlob->Release();
				}
				if (sigBlob != NULL) sigBlob->Release();
				FreeAndExit("Couldn't serialize local raytracing root signature(hw)");
			}
			if (device->CreateRootSignature(0, sigBlob->GetBufferPointer(), sigBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void **)&localRTRootSignature) != S_OK) {
				sigBlob->Release();
				FreeAndExit("Couldn't create local raytracing root signature(hw)");
			}
			if (errBlob != NULL) errBlob->Release();
			sigBlob->Release();
		}
	}

	{} /* Create raytracing pipeline state object */ {
		// RTPSO consists of 7 subobjects - associated with DXIL exports(shaders) by way of 
		// default or explicit associations.  Default association applies to every exported shader 
		// entrypoint that doesn't have any of the same type of subobject associated with it.
		// This sample USES default association except for THE local root signature subobject
		// which has an explicit association for demonstration purposes.
		// 1 - DXIL library
		// 1 - Triangle hit group
		// 1 - Shader config
		// 2 - Local root signature and association
		// 1 - Global root signature
		// 1 - Pipeline config

		D3D12_STATE_SUBOBJECT subs[7];   // State subobject array
		D3D12_STATE_OBJECT_DESC sodesc;  // State object description
		sodesc.Type = D3D12_STATE_OBJECT_TYPE_RAYTRACING_PIPELINE;
		sodesc.NumSubobjects = 7;
		sodesc.pSubobjects = subs;

		// 1: Sub-object 0 is the DXIL shader code library
		D3D12_EXPORT_DESC exports[3];
		D3D12_DXIL_LIBRARY_DESC lib;

		exports[0].Name = L"MyRaygenShader";
		exports[0].ExportToRename = NULL;
		exports[0].Flags = D3D12_EXPORT_FLAG_NONE;
		exports[1].Name = L"MyClosestHitShader";
		exports[1].ExportToRename = NULL;
		exports[1].Flags = D3D12_EXPORT_FLAG_NONE;
		exports[2].Name = L"MyMissShader";
		exports[2].ExportToRename = NULL;
		exports[2].Flags = D3D12_EXPORT_FLAG_NONE;

		lib.DXILLibrary.pShaderBytecode = &g_pRaytracing;
		lib.DXILLibrary.BytecodeLength = sizeof(g_pRaytracing);
		lib.NumExports = 3;
		lib.pExports = exports;

		subs[0].Type = D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY;
		subs[0].pDesc = &lib;

		// 2: Sub-object 1 is a triangle hit group (shaders to be used on ray intersection)
		D3D12_HIT_GROUP_DESC hits;
		hits.HitGroupExport = L"MyHitGroup";
		hits.Type = D3D12_HIT_GROUP_TYPE_TRIANGLES;
		hits.AnyHitShaderImport = NULL;
		hits.ClosestHitShaderImport = L"MyClosestHitShader";
		hits.IntersectionShaderImport = NULL;
		subs[1].Type = D3D12_STATE_SUBOBJECT_TYPE_HIT_GROUP;
		subs[1].pDesc = &hits;

		// 3: Sub-object 2 is the shader configuration (max sizes for ray payload and attribute struct)
		D3D12_RAYTRACING_SHADER_CONFIG sconfig;
		sconfig.MaxPayloadSizeInBytes = 4 * sizeof(float);   // Colour
		sconfig.MaxAttributeSizeInBytes = 2 * sizeof(float); // Barycentric coordinates
		subs[2].Type = D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_SHADER_CONFIG;
		subs[2].pDesc = &sconfig;

		// 4: Sub-object 3 is local root signature
		D3D12_LOCAL_ROOT_SIGNATURE localrs;
		localrs.pLocalRootSignature = localRTRootSignature;

		subs[3].Type = D3D12_STATE_SUBOBJECT_TYPE_LOCAL_ROOT_SIGNATURE;
		subs[3].pDesc = &localrs;

		// 5: Sub-object 4 is local root signature shader association
		LPCWSTR names[] = { L"MyRaygenShader" };
		D3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION subexport;
		subexport.pSubobjectToAssociate = &subs[3];
		subexport.NumExports = 1;
		subexport.pExports = names;

		subs[4].Type = D3D12_STATE_SUBOBJECT_TYPE_SUBOBJECT_TO_EXPORTS_ASSOCIATION;
		subs[4].pDesc = &subexport;

		// 6: Sub-object 5 is global root signature (shared by all shaders during DispatchRays call)
		D3D12_GLOBAL_ROOT_SIGNATURE globalrs;
		globalrs.pGlobalRootSignature = globalRTRootSignature;
		subs[5].Type = D3D12_STATE_SUBOBJECT_TYPE_GLOBAL_ROOT_SIGNATURE;
		subs[5].pDesc = &globalrs;

		// 7: Sub-object 6 is the pipeline config - defining the maximum TraceRay() recursion depth
		D3D12_RAYTRACING_PIPELINE_CONFIG pconfig;
		pconfig.MaxTraceRecursionDepth = 2;  // No secondary rays

		subs[6].Type = D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_PIPELINE_CONFIG;
		subs[6].pDesc = &pconfig;

		//PrintStateObjectDesc(raytracingPipeline);  // TODO - implement this
		printf("About to create the raytracing state object\n");
		DWORD res = device->CreateStateObject(&sodesc, __uuidof(ID3D12StateObject), (void **)&rtStateObject);
		if (res != S_OK) {
			printf("Error code %x\n", res);
			FreeAndExit("Couldn't create raytracing state object\n");
		}

		//if (device->CreateStateObject(&sodesc, __uuidof(ID3D12StateObject), (void **)&rtStateObject) != S_OK)
		//	FreeAndExit("Couldn't create raytracing state object\n");
	}

	/* Create vertex buffer */ {
		CreateVertexBuffer("cube.obj", vertexBuffer, vertexCount);
	}

	CreateConstantBuffer();
	CreateTexture();

	{} /* Create geometry for ray tracing (rtVertexBuffer and rtIndexBuffer) */ {
	//	const float depthValue = 1.0f, offset = 0.7f;
	//	//const float s = 1.0;
	//	//Vertex bigvertices[] = {
	//	//	{{1, 1, 1}, {0, 1, 0}, {0, 0}},
	//	//	{{1, 1, 1}, {0, 1, 0}, {0, 1}}
	//	//};
	//	RTVertex vertices[] = { {{ 0, -offset, depthValue }}, {{ -offset, offset, depthValue }}, {{ offset, offset, depthValue }},
	//							{{ 0.9f, -0.8f, 1.5f }}, {{ -0.9f, -0.8f, 1.5f }}, {{ 0, 0.0f, 0.5f }} };
	//	UINT16 indices[] = { 0, 1, 2, 3, 4, 5 };
	//	
	//	// Upload vertex data
	//	D3D12_HEAP_PROPERTIES vbhprop;
	//	vbhprop.Type = D3D12_HEAP_TYPE_UPLOAD;
	//	vbhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	//	vbhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	//	vbhprop.CreationNodeMask = 0;
	//	vbhprop.VisibleNodeMask = 0;
	//	
	//	D3D12_RESOURCE_DESC vbdesc;
	//	vbdesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	//	vbdesc.Alignment = 0;
	//	vbdesc.Width = sizeof(vertices);
	//	vbdesc.Height = 1;
	//	vbdesc.DepthOrArraySize = 1;
	//	vbdesc.MipLevels = 1;
	//	vbdesc.Format = DXGI_FORMAT_UNKNOWN;
	//	vbdesc.SampleDesc.Count = 1;
	//	vbdesc.SampleDesc.Quality = 0;
	//	vbdesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	//	vbdesc.Flags = D3D12_RESOURCE_FLAG_NONE;
	//	
	//	if (device->CreateCommittedResource(
	//		&vbhprop, D3D12_HEAP_FLAG_NONE, &vbdesc,
	//		D3D12_RESOURCE_STATE_GENERIC_READ, NULL,
	//		__uuidof(ID3D12Resource), (void **)&rtVertexBuffer) != S_OK) {
	//		FreeAndExit("Couldn't create ray tracing vertex buffer");
	//	}
	//	
	//	UINT8 *vertexBufferPtr;
	//	D3D12_RANGE readRange;
	//	readRange.Begin = 0;
	//	readRange.End = 0;
	//	if (rtVertexBuffer->Map(0, &readRange, (void **)&vertexBufferPtr) != S_OK) {
	//		FreeAndExit("Couldn't map ray tracing vertex buffer");
	//	}
	//	memcpy(vertexBufferPtr, vertices, sizeof(vertices));
	//	rtVertexBuffer->Unmap(0, NULL);

	//	// Upload index data
	//	vbdesc.Width = sizeof(indices);
	//	
	//	if (device->CreateCommittedResource(
	//		&vbhprop, D3D12_HEAP_FLAG_NONE, &vbdesc,
	//		D3D12_RESOURCE_STATE_GENERIC_READ, NULL,
	//		__uuidof(ID3D12Resource), (void **)&rtIndexBuffer) != S_OK) {
	//		FreeAndExit("Couldn't create ray tracing index buffer");
	//	}
	//	
	//	UINT8 *indexBufferPtr;
	//	if (rtIndexBuffer->Map(0, &readRange, (void **)&indexBufferPtr) != S_OK) {
	//		FreeAndExit("Couldn't map ray tracing index buffer");
	//	}
	//	memcpy(indexBufferPtr, indices, sizeof(indices));
	//	rtIndexBuffer->Unmap(0, NULL);
	}

	/* Create acceleration structures */ {
		// CommandList is open and has texture copy command in the list already

		D3D12_RAYTRACING_GEOMETRY_DESC geometryDesc; {
			geometryDesc.Type = D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
			geometryDesc.Flags = D3D12_RAYTRACING_GEOMETRY_FLAG_OPAQUE;
			geometryDesc.Triangles.Transform3x4 = NULL;
			geometryDesc.Triangles.IndexFormat = DXGI_FORMAT_UNKNOWN; // DXGI_FORMAT_R16_UINT;
			geometryDesc.Triangles.VertexFormat = DXGI_FORMAT_R32G32B32_FLOAT;
			geometryDesc.Triangles.IndexCount = 0;
			geometryDesc.Triangles.VertexCount = vertexCount;
			geometryDesc.Triangles.IndexBuffer = NULL; // rtIndexBuffer->GetGPUVirtualAddress();
			geometryDesc.Triangles.VertexBuffer.StartAddress = vertexBuffer->GetGPUVirtualAddress();
			geometryDesc.Triangles.VertexBuffer.StrideInBytes = vertexSize; // sizeof(RTVertex);
		}

		D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS topLevelInputs;
		D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO topPreBuild;
		/* Get memory requirements for top level acceleration structure */ {
			topLevelInputs.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;
			topLevelInputs.Flags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
			topLevelInputs.NumDescs = 4;
			topLevelInputs.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
			topLevelInputs.InstanceDescs = NULL;  // Will be filled when description structure is created

			device->GetRaytracingAccelerationStructurePrebuildInfo(&topLevelInputs, &topPreBuild);
			if (topPreBuild.ResultDataMaxSizeInBytes <= 0)
				FreeAndExit("Couldn't determine memory requirements for building top level acceleration structure");
		}

		D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS bottomLevelInputs;
		D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO bottomPreBuild;
		/* Get memory requirements for bottom level acceleration structure */ {
			bottomLevelInputs.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL;
			bottomLevelInputs.Flags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
			bottomLevelInputs.NumDescs = 1;
			bottomLevelInputs.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
			bottomLevelInputs.pGeometryDescs = &geometryDesc;

			device->GetRaytracingAccelerationStructurePrebuildInfo(&bottomLevelInputs, &bottomPreBuild);
			if (bottomPreBuild.ResultDataMaxSizeInBytes <= 0)
				FreeAndExit("Couldn't determine memory requirements for building bottom level acceleration structure");
		}

		/* Allocate resources for scratch area, and top/bottom level acceleration structures */ {
			D3D12_HEAP_PROPERTIES sbhprop;
			sbhprop.Type = D3D12_HEAP_TYPE_DEFAULT;
			sbhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
			sbhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
			sbhprop.CreationNodeMask = 0;
			sbhprop.VisibleNodeMask = 0;

			D3D12_RESOURCE_DESC rdesc;
			rdesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
			rdesc.Alignment = 0;
			rdesc.Width = max(topPreBuild.ScratchDataSizeInBytes, bottomPreBuild.ScratchDataSizeInBytes);
			rdesc.Height = 1;
			rdesc.DepthOrArraySize = 1;
			rdesc.MipLevels = 1;
			rdesc.Format = DXGI_FORMAT_UNKNOWN;
			rdesc.SampleDesc.Count = 1;
			rdesc.SampleDesc.Quality = 0;
			rdesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
			rdesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;

			if (device->CreateCommittedResource(&sbhprop, D3D12_HEAP_FLAG_NONE, &rdesc, D3D12_RESOURCE_STATE_UNORDERED_ACCESS, NULL, __uuidof(ID3D12Resource), (void **)&scratchArea) != S_OK)
				FreeAndExit("Couldn't allocate scratch area for raytracing acceleration construction");

			D3D12_RESOURCE_STATES initialAccResState;
			initialAccResState = D3D12_RESOURCE_STATE_RAYTRACING_ACCELERATION_STRUCTURE;

			rdesc.Width = topPreBuild.ResultDataMaxSizeInBytes;
			if (device->CreateCommittedResource(&sbhprop, D3D12_HEAP_FLAG_NONE, &rdesc, initialAccResState, NULL, __uuidof(ID3D12Resource), (void **)&topLevelAccStr) != S_OK)
				FreeAndExit("Couldn't allocate buffer for top level acceleration structure");
			rdesc.Width = bottomPreBuild.ResultDataMaxSizeInBytes;
			if (device->CreateCommittedResource(&sbhprop, D3D12_HEAP_FLAG_NONE, &rdesc, initialAccResState, NULL, __uuidof(ID3D12Resource), (void **)&bottomLevelAccStr) != S_OK)
				FreeAndExit("Couldn't allocate buffer for top level acceleration structure");
		}

		/* Create instance descriptions */ {
			D3D12_RAYTRACING_INSTANCE_DESC instdescs[4];
			float angle = 90.0f * 3.14159f / 180.0f;
			float cAn = (float)cos(angle);  float sAn = (float)sin(angle);
			instdescs[0].Transform[0][0] = +cAn; instdescs[0].Transform[1][0] = 0.0f; instdescs[0].Transform[2][0] = -sAn;
			instdescs[0].Transform[0][1] = 0.0f; instdescs[0].Transform[1][1] = 1.0f; instdescs[0].Transform[2][1] = 0.0f;
			instdescs[0].Transform[0][2] = +sAn; instdescs[0].Transform[1][2] = 0.0f; instdescs[0].Transform[2][2] = +cAn;
			instdescs[0].Transform[0][3] = 0.0f; instdescs[0].Transform[1][3] = 0.0f; instdescs[0].Transform[2][3] = 0.0f;
			instdescs[0].InstanceID = 0;
			instdescs[0].InstanceMask = 1;
			instdescs[0].InstanceContributionToHitGroupIndex = 0;
			instdescs[0].Flags = D3D12_RAYTRACING_INSTANCE_FLAG_NONE;
			instdescs[0].AccelerationStructure = bottomLevelAccStr->GetGPUVirtualAddress();

			instdescs[1].Transform[0][0] = 1.0f; instdescs[1].Transform[1][0] = 0.0f; instdescs[1].Transform[2][0] = 0.0f;
			instdescs[1].Transform[0][1] = 0.0f; instdescs[1].Transform[1][1] = 1.0f; instdescs[1].Transform[2][1] = 0.0f;
			instdescs[1].Transform[0][2] = 0.0f; instdescs[1].Transform[1][2] = 0.0f; instdescs[1].Transform[2][2] = 1.0f;
			instdescs[1].Transform[0][3] = 1.3f; instdescs[1].Transform[1][3] = 1.3f; instdescs[1].Transform[2][3] = 1.3f;
			instdescs[1].InstanceID = 0;
			instdescs[1].InstanceMask = 1;
			instdescs[1].InstanceContributionToHitGroupIndex = 0;
			instdescs[1].Flags = D3D12_RAYTRACING_INSTANCE_FLAG_NONE;
			instdescs[1].AccelerationStructure = bottomLevelAccStr->GetGPUVirtualAddress();

			instdescs[2].Transform[0][0] = 1.0f; instdescs[2].Transform[1][0] = 0.0f; instdescs[2].Transform[2][0] = 0.0f;
			instdescs[2].Transform[0][1] = 0.0f; instdescs[2].Transform[1][1] = 1.0f; instdescs[2].Transform[2][1] = 0.0f;
			instdescs[2].Transform[0][2] = 0.0f; instdescs[2].Transform[1][2] = 0.0f; instdescs[2].Transform[2][2] = 1.0f;
			instdescs[2].Transform[0][3] = -1.3f; instdescs[2].Transform[1][3] = -1.3f; instdescs[2].Transform[2][3] = 0.5f;
			instdescs[2].InstanceID = 0;
			instdescs[2].InstanceMask = 1;
			instdescs[2].InstanceContributionToHitGroupIndex = 0;
			instdescs[2].Flags = D3D12_RAYTRACING_INSTANCE_FLAG_NONE;
			instdescs[2].AccelerationStructure = bottomLevelAccStr->GetGPUVirtualAddress();

			instdescs[3].Transform[0][0] = 5.0f; instdescs[3].Transform[1][0] = 0.0f; instdescs[3].Transform[2][0] = 0.0f;
			instdescs[3].Transform[0][1] = 0.0f; instdescs[3].Transform[1][1] = 5.0f; instdescs[3].Transform[2][1] = 0.0f;
			instdescs[3].Transform[0][2] = 0.0f; instdescs[3].Transform[1][2] = 0.0f; instdescs[3].Transform[2][2] = 1.0f;
			instdescs[3].Transform[0][3] = 0.0f; instdescs[3].Transform[1][3] = 0.0f; instdescs[3].Transform[2][3] = -5.0f;
			instdescs[3].InstanceID = 0;
			instdescs[3].InstanceMask = 1;
			instdescs[3].InstanceContributionToHitGroupIndex = 0;
			instdescs[3].Flags = D3D12_RAYTRACING_INSTANCE_FLAG_NONE;
			instdescs[3].AccelerationStructure = bottomLevelAccStr->GetGPUVirtualAddress();

			// Allocate GPU resource for instance description
			D3D12_HEAP_PROPERTIES idhprop;
			idhprop.Type = D3D12_HEAP_TYPE_UPLOAD;
			idhprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
			idhprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
			idhprop.CreationNodeMask = 0;
			idhprop.VisibleNodeMask = 0;

			D3D12_RESOURCE_DESC idrdesc;
			idrdesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
			idrdesc.Alignment = 0;
			idrdesc.Width = sizeof(instdescs);
			idrdesc.Height = 1;
			idrdesc.DepthOrArraySize = 1;
			idrdesc.MipLevels = 1;
			idrdesc.Format = DXGI_FORMAT_UNKNOWN;
			idrdesc.SampleDesc.Count = 1;
			idrdesc.SampleDesc.Quality = 0;
			idrdesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
			idrdesc.Flags = D3D12_RESOURCE_FLAG_NONE;

			if (device->CreateCommittedResource(&idhprop, D3D12_HEAP_FLAG_NONE, &idrdesc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL, __uuidof(ID3D12Resource), (void **)&instanceDescription) != S_OK)
				FreeAndExit("Couldn't allocate acceleration structure instance description");

			// Copy instance description to GPU memory
			unsigned char *descPtr;
			instanceDescription->Map(0, NULL, (void **)&descPtr);
			memcpy(descPtr, &instdescs, sizeof(instdescs));
			instanceDescription->Unmap(0, NULL);

			// Make instance descriptions available for construction of top level structure
			topLevelInputs.InstanceDescs = instanceDescription->GetGPUVirtualAddress();
		}

		{} /* Schedule commands to construct acceleration structures on GPU */ {
			D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC bottombuilddesc;
			bottombuilddesc.DestAccelerationStructureData = bottomLevelAccStr->GetGPUVirtualAddress();
			bottombuilddesc.Inputs = bottomLevelInputs;
			bottombuilddesc.SourceAccelerationStructureData = NULL;
			bottombuilddesc.ScratchAccelerationStructureData = scratchArea->GetGPUVirtualAddress();

			D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC topbuilddesc;
			topbuilddesc.DestAccelerationStructureData = topLevelAccStr->GetGPUVirtualAddress();
			topbuilddesc.Inputs = topLevelInputs;
			topbuilddesc.SourceAccelerationStructureData = NULL;
			topbuilddesc.ScratchAccelerationStructureData = scratchArea->GetGPUVirtualAddress();

			D3D12_RESOURCE_BARRIER barrier;
			barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_UAV;
			barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			barrier.UAV.pResource = bottomLevelAccStr;

			commandList->BuildRaytracingAccelerationStructure(&bottombuilddesc, 0, NULL);
			commandList->ResourceBarrier(1, &barrier);
			commandList->BuildRaytracingAccelerationStructure(&topbuilddesc, 0, NULL);
		}
	}

	{} /* Create shader tables for ray tracing */ {
		// Find id size and ids for shaders
		UINT shaderidsize;
		void *raygenid;
		void *missid;
		void *hitgroupid;
		ID3D12StateObjectProperties *stateObjProps;
		if (rtStateObject->QueryInterface(__uuidof(ID3D12StateObjectProperties), (void **)&stateObjProps) != S_OK)
			FreeAndExit("Couldn't obtain ray tracing state object properties interface");
		shaderidsize = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES;
		raygenid = stateObjProps->GetShaderIdentifier(L"MyRaygenShader");
		missid = stateObjProps->GetShaderIdentifier(L"MyMissShader");
		hitgroupid = stateObjProps->GetShaderIdentifier(L"MyHitGroup");
		stateObjProps->Release();

		// Descriptors for allocating shader tables
		D3D12_HEAP_PROPERTIES sthprop;
		sthprop.Type = D3D12_HEAP_TYPE_UPLOAD;
		sthprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		sthprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		sthprop.CreationNodeMask = 0;
		sthprop.VisibleNodeMask = 0;

		D3D12_RESOURCE_DESC stdesc;
		stdesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
		stdesc.Alignment = 0;
		//stdesc.Width to be filled later;
		stdesc.Height = 1;
		stdesc.DepthOrArraySize = 1;
		stdesc.MipLevels = 1;
		stdesc.Format = DXGI_FORMAT_UNKNOWN;
		stdesc.SampleDesc.Count = 1;
		stdesc.SampleDesc.Quality = 0;
		stdesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		stdesc.Flags = D3D12_RESOURCE_FLAG_NONE;

		UINT shaderrecsize;
		byte *stPtr;
		// Ray gen shader table
		shaderrecsize = RoundUp(shaderidsize + sizeof(D3D12_GPU_VIRTUAL_ADDRESS) + sizeof(RayGenConstantBuffer), D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT);
		stdesc.Width = shaderrecsize;  // only one shader record
		if (device->CreateCommittedResource(&sthprop, D3D12_HEAP_FLAG_NONE, &stdesc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL, __uuidof(ID3D12Resource), (void **)&raygenShaderTable) != S_OK)
			FreeAndExit("Couldn't allocate resource for ray generation shader table");
		raygenShaderTable->Map(0, NULL, (void **)&stPtr);		// This mapping is left open for dynamic update
		memcpy(stPtr, raygenid, shaderidsize);										// Fill the shader id
		stPtr += shaderidsize;
		D3D12_GPU_VIRTUAL_ADDRESS cbaddr = constantBuffer->GetGPUVirtualAddress();	// Fill the constant buffer reference
		memcpy(stPtr, &cbaddr, sizeof(cbaddr));
		stPtr += sizeof(cbaddr);													
		rtConstantBufferMappedPointer = stPtr;
		memcpy(stPtr, &raygenConstantBufferData, sizeof(RayGenConstantBuffer));		// Fill the rt constants in place

		// Miss shader table
		shaderrecsize = RoundUp(shaderidsize, D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT);
		stdesc.Width = shaderrecsize;  // only one shader record
		if (device->CreateCommittedResource(&sthprop, D3D12_HEAP_FLAG_NONE, &stdesc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL, __uuidof(ID3D12Resource), (void **)&missShaderTable) != S_OK)
			FreeAndExit("Couldn't allocate resource for miss shader table");
		missShaderTable->Map(0, NULL, (void **)&stPtr);
		memcpy(stPtr, missid, shaderidsize);
		missShaderTable->Unmap(0, NULL);

		// Hit group shader table
		shaderrecsize = RoundUp(shaderidsize, D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT);
		stdesc.Width = shaderrecsize;  // only one shader record
		if (device->CreateCommittedResource(&sthprop, D3D12_HEAP_FLAG_NONE, &stdesc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL, __uuidof(ID3D12Resource), (void **)&hitgroupShaderTable) != S_OK)
			FreeAndExit("Couldn't allocate resource for hit group shader table");
		hitgroupShaderTable->Map(0, NULL, (void **)&stPtr);
		memcpy(stPtr, hitgroupid, shaderidsize);
		hitgroupShaderTable->Unmap(0, NULL);
	}

	/* Create UAV buffer for ray tracing output */ {
		D3D12_HEAP_PROPERTIES rtohprop;
		rtohprop.Type = D3D12_HEAP_TYPE_DEFAULT;
		rtohprop.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		rtohprop.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		rtohprop.CreationNodeMask = 0;
		rtohprop.VisibleNodeMask = 0;

		D3D12_RESOURCE_DESC rtodesc;
		rtodesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
		rtodesc.Alignment = 0;
		rtodesc.Width = WIDTH;
		rtodesc.Height = HEIGHT;
		rtodesc.DepthOrArraySize = 1;
		rtodesc.MipLevels = 1;
		rtodesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		rtodesc.SampleDesc.Count = 1;
		rtodesc.SampleDesc.Quality = 0;
		rtodesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
		rtodesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;

		if (device->CreateCommittedResource(&rtohprop, D3D12_HEAP_FLAG_NONE, &rtodesc, D3D12_RESOURCE_STATE_UNORDERED_ACCESS, NULL, __uuidof(ID3D12Resource), (void **)&rtOutputBuffer) != S_OK)
			FreeAndExit("Couldn't allocate ray tracing output buffer");

		// Create unordered access view and save the GPU descriptor of the view
		D3D12_UNORDERED_ACCESS_VIEW_DESC vdesc;
		vdesc.Format = DXGI_FORMAT_UNKNOWN;
		vdesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2D;
		vdesc.Texture2D.MipSlice = 0;
		vdesc.Texture2D.PlaneSlice = 0;
		device->CreateUnorderedAccessView(rtOutputBuffer, NULL, &vdesc, rtHeap->GetCPUDescriptorHandleForHeapStart());
	}
}

void Init()
{
	////////////////////////// PIPELINE ///////////////////////////
	CreateDirectX12Pipeline();

	/* Create command list (getting more recent version for ray tracing) */ {
		ID3D12GraphicsCommandList *tempCL;
		if (device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, directCommandAllocator[backBufferIndex], NULL, __uuidof(ID3D12GraphicsCommandList), (void **)&tempCL) != S_OK)
			FreeAndExit("Couldn't create command list");
		if (tempCL->QueryInterface(__uuidof(ID3D12GraphicsCommandList4), (void **)&commandList) != S_OK) {
			tempCL->Release();
			FreeAndExit("Couldn't get DirectX Raytracing interface for the command list");
		}
		tempCL->Release();
	}

	/////////////////////////// ASSETS ////////////////////////////
	CreateAssetsRayTracing();

	//----------------------------------------------------------------------------------------

	/* Close command list (containing acc str creation) and execute commands */ {
		if (commandList->Close() != S_OK)
			FreeAndExit("Couldn't close command list to process buffer copies");
		ID3D12CommandList *cl[] = { commandList };
		commandQueue->ExecuteCommandLists(1, cl);
	}

	/* Create synchronisation objects */ {
		if (device->CreateFence(bufferFenceValue[backBufferIndex], D3D12_FENCE_FLAG_NONE, __uuidof(ID3D12Fence), (void **)&fence) != S_OK)
			FreeAndExit("Couldn't create fence");
		bufferFenceValue[backBufferIndex]++;
		fenceEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		if (fenceEvent == NULL)
			FreeAndExit("Couldn't create event for fence");
	}

	WaitForCommandCompletion(bufferFenceValue[backBufferIndex]);
	bufferFenceValue[backBufferIndex]++;
}

void RayTracingFillCommandList()
{
	// Can only be done when command lists have finished execution
	if (directCommandAllocator[backBufferIndex]->Reset() != S_OK)
		FreeAndExit("Can't reset command allocator");

	// Can be done as soon as execute has been called
	if (commandList->Reset(directCommandAllocator[backBufferIndex], NULL) != S_OK)
		FreeAndExit("Can't reset command list");

	//ID3D12DescriptorHeap *heaps[2] = { rtHeap, srvHeap };
	commandList->SetComputeRootSignature(globalRTRootSignature);
	commandList->SetDescriptorHeaps(1, &rtHeap);
	D3D12_GPU_DESCRIPTOR_HANDLE rthandle = rtHeap->GetGPUDescriptorHandleForHeapStart();
	commandList->SetComputeRootDescriptorTable(0, rthandle);
	commandList->SetComputeRootShaderResourceView(1, topLevelAccStr->GetGPUVirtualAddress());
	commandList->SetComputeRootShaderResourceView(2, vertexBuffer->GetGPUVirtualAddress()); // <== Not in rtHeap - but directly set here
	rthandle.ptr += rtDescriptorSize;
	commandList->SetComputeRootDescriptorTable(3, rthandle);

	D3D12_DISPATCH_RAYS_DESC dispatchdesc;
	dispatchdesc.RayGenerationShaderRecord.StartAddress = raygenShaderTable->GetGPUVirtualAddress();
	dispatchdesc.RayGenerationShaderRecord.SizeInBytes = raygenShaderTable->GetDesc().Width;
	dispatchdesc.MissShaderTable.StartAddress = missShaderTable->GetGPUVirtualAddress();
	dispatchdesc.MissShaderTable.SizeInBytes = missShaderTable->GetDesc().Width;
	dispatchdesc.MissShaderTable.StrideInBytes = dispatchdesc.MissShaderTable.SizeInBytes;
	dispatchdesc.HitGroupTable.StartAddress = hitgroupShaderTable->GetGPUVirtualAddress();
	dispatchdesc.HitGroupTable.SizeInBytes = hitgroupShaderTable->GetDesc().Width;
	dispatchdesc.HitGroupTable.StrideInBytes = dispatchdesc.HitGroupTable.SizeInBytes;
	dispatchdesc.CallableShaderTable.StartAddress = 0;
	dispatchdesc.CallableShaderTable.SizeInBytes = 0;
	dispatchdesc.CallableShaderTable.StrideInBytes = 0;
	dispatchdesc.Width = WIDTH;
	dispatchdesc.Height = HEIGHT;
	dispatchdesc.Depth = 1;

	commandList->SetPipelineState1(rtStateObject);
	commandList->DispatchRays(&dispatchdesc);

	/* Copy the raytracing output to the backbuffer */ {
	
		/* Resource barrier render target to copy destination */ {
			D3D12_RESOURCE_BARRIER preCopyBarriers[2];
			preCopyBarriers[0].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			preCopyBarriers[0].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			preCopyBarriers[0].Transition.pResource = renderTargets[backBufferIndex];
			preCopyBarriers[0].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			preCopyBarriers[0].Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
			preCopyBarriers[0].Transition.StateAfter = D3D12_RESOURCE_STATE_COPY_DEST;
			preCopyBarriers[1].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			preCopyBarriers[1].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			preCopyBarriers[1].Transition.pResource = rtOutputBuffer;
			preCopyBarriers[1].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			preCopyBarriers[1].Transition.StateBefore = D3D12_RESOURCE_STATE_UNORDERED_ACCESS;
			preCopyBarriers[1].Transition.StateAfter = D3D12_RESOURCE_STATE_COPY_SOURCE;
			commandList->ResourceBarrier(2, preCopyBarriers);
		}

		commandList->CopyResource(renderTargets[backBufferIndex], rtOutputBuffer);

		/* Resource barrier render target to presentation state */	{
			D3D12_RESOURCE_BARRIER postCopyBarriers[2];
			postCopyBarriers[0].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			postCopyBarriers[0].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			postCopyBarriers[0].Transition.pResource = renderTargets[backBufferIndex];
			postCopyBarriers[0].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			postCopyBarriers[0].Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
			postCopyBarriers[0].Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
			postCopyBarriers[1].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			postCopyBarriers[1].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			postCopyBarriers[1].Transition.pResource = rtOutputBuffer;
			postCopyBarriers[1].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			postCopyBarriers[1].Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_SOURCE;
			postCopyBarriers[1].Transition.StateAfter = D3D12_RESOURCE_STATE_UNORDERED_ACCESS;
			commandList->ResourceBarrier(2, postCopyBarriers);
		}
	}

	// That's the end of the command list
	if (commandList->Close() != S_OK)
		FreeAndExit("Couldn't close command list");
}

void ResetNavigation()
{
	heading = 0.0f;
	elevation = 0.0f;
	cameraPosition = { 0.0f, 0.0f, 7.0f };
}

void Update(float timeSecs, float deltaTime)
{
	if (keyMap[VK_ESCAPE]) PostMessage(MainWindowHandle, WM_CLOSE, 0, 0);
	if (keyMap['R']) {
		ResetNavigation();
		keyMap['R'] = false;
	}

	const float TURN_RATE = 0.01f * deltaTime * 60.0f, MOVE_RATE = 0.1f * deltaTime * 60.0f;
	if (keyMap[VK_UP]) elevation += TURN_RATE;
	if (keyMap[VK_DOWN]) elevation -= TURN_RATE;
	if (keyMap[VK_LEFT]) heading += TURN_RATE;
	if (keyMap[VK_RIGHT]) heading -= TURN_RATE;
	XMVECTOR forward, up, right;
	forward = XMVector3Transform({ 0.0f, 0.0f, -1.0f }, XMMatrixMultiply(XMMatrixRotationX(elevation), XMMatrixRotationY(heading)));
	right = XMVector3Normalize(XMVector3Cross(forward, { 0.0f, 1.0f, 0.0f }));
	up = XMVector3Cross(right, forward);
	if (keyMap['W']) cameraPosition = XMVectorAdd(cameraPosition, XMVectorScale(forward, MOVE_RATE));
	if (keyMap['S']) cameraPosition = XMVectorSubtract(cameraPosition, XMVectorScale(forward, MOVE_RATE));
	if (keyMap['A']) cameraPosition = XMVectorSubtract(cameraPosition, XMVectorScale(right, MOVE_RATE));
	if (keyMap['D']) cameraPosition = XMVectorAdd(cameraPosition, XMVectorScale(right, MOVE_RATE));
	if (keyMap['Q']) cameraPosition = XMVectorAdd(cameraPosition, XMVectorScale(up, MOVE_RATE));
	if (keyMap['Z']) cameraPosition = XMVectorSubtract(cameraPosition, XMVectorScale(up, MOVE_RATE));

	constantBufferData.cameraPosition = cameraPosition;
	constantBufferData.forward = forward;
	constantBufferData.right = right;
	constantBufferData.up = up;
	constantBufferData.seconds = timeSecs;
	memcpy(constantBufferMappedPointer, &constantBufferData, sizeof(ConstantBuffer));

	//raygenConstantBufferData.viewport.left -= 0.001f;
	//raygenConstantBufferData.viewport.right -= 0.001f;
	//memcpy(rtConstantBufferMappedPointer, &raygenConstantBufferData, sizeof(RayGenConstantBuffer));
}

void Render()
{
	static int frameNumber = 0;
	frameNumber++;
	RayTracingFillCommandList();
	ID3D12CommandList *cl[] = { commandList };
	commandQueue->ExecuteCommandLists(1, cl);
	UINT64 fenceValue = bufferFenceValue[backBufferIndex];
	WaitForCommandCompletion(bufferFenceValue[backBufferIndex]);

	DWORD err;
	if ((err = swapChain->Present(1, 0)) != S_OK) {
		printf("Failure to present frame %d, error code %x\n", frameNumber, err);
		FreeAndExit("Couldn't present frame");
	}

	backBufferIndex = swapChain->GetCurrentBackBufferIndex();
	bufferFenceValue[backBufferIndex] = fenceValue + 1;
}

void ReportFrameRate(float nowSecs, float timeInterval, int numReports)
{
	// Will issue a total of numReports reports.
	// Set numReports to -1 to report forever, 0 never.
	static int frameCount = -1;
	static int reportCount = 0;
	static float startSecs;

	if (reportCount == numReports) return;

	if (frameCount < 0) {
		startSecs = nowSecs;
		frameCount = 0;
	}

	frameCount++;
	if (nowSecs - startSecs < timeInterval) return;

	float elapsedSecs = (float)(nowSecs - startSecs);
	float fps = ((float)frameCount) / elapsedSecs;
	printf("Time for %d frames is %0.000fs (%0.1f fps)\n", frameCount, elapsedSecs, fps);
	reportCount++;
	startSecs = nowSecs;
	frameCount = 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	switch (message)
	{
	case WM_ERASEBKGND:
		return 0;
	case WM_PAINT:
		BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;
	case WM_KEYDOWN:
		keyMap[wParam & 255] = true;
		break;
	case WM_KEYUP:
		keyMap[wParam & 255] = false;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	AllocConsole();
	FILE *outstream, *instream;
	freopen_s(&outstream, "con:", "w", stdout);
	freopen_s(&instream, "con:", "r", stdin);

	{	// Register window class
		WNDCLASSEXW wcex;
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = NULL;
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)NULL;
		wcex.lpszMenuName = NULL;
		wcex.lpszClassName = L"RayTwo01Class";
		wcex.hIconSm = NULL;
		RegisterClassExW(&wcex);
	}

	RECT area = { 0, 0, WIDTH, HEIGHT };
	AdjustWindowRectEx(&area, WS_OVERLAPPEDWINDOW, NULL, 0);
	MainWindowHandle = CreateWindowW(L"RayTwo01Class", L"RayTwo01", WS_OVERLAPPEDWINDOW,
		area.left + 800, area.top + 40, area.right - area.left, area.bottom - area.top,
		nullptr, nullptr, hInstance, nullptr);
	if (MainWindowHandle == NULL) return false;
	ShowWindow(MainWindowHandle, nCmdShow);

	Init();

	// Time variables and initialisation
	float timeSecs;
	FILETIME nowTime;
	UINT64 start64, now64;
	GetSystemTimeAsFileTime(&nowTime);
	start64 = ((UINT64)(nowTime.dwHighDateTime)) * 0x100000000 + ((UINT64)(nowTime.dwLowDateTime));
	timeSecs = 0.0f;

	// Main message loop
	MSG msg;
	while (true) {
		if (PeekMessage(&msg, nullptr, 0, 0, TRUE)) {
			if (msg.message == WM_QUIT) break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			GetSystemTimeAsFileTime(&nowTime);
			now64 = ((UINT64)(nowTime.dwHighDateTime)) * 0x100000000 + ((UINT64)(nowTime.dwLowDateTime));
			float lastTime = timeSecs;
			timeSecs = ((float)((now64 - start64) / 10000)) / 1000.0f;

			Update(timeSecs, timeSecs - lastTime);
			Render();
			ReportFrameRate(timeSecs, 5.0f, 5);
		}
	}

	WaitForCommandCompletion(bufferFenceValue[backBufferIndex]);
	FreeResources();

	return (int)msg.wParam;
}
