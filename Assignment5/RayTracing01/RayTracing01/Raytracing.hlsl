struct Vertex
{
	float3 position;
	float3 normal;
	float2 texCoord;
};

RaytracingAccelerationStructure Scene : register(t0);
RWTexture2D<float4> RenderTarget : register(u0);
StructuredBuffer<Vertex> vertices : register(t1);
Texture2D g_texture : register(t2);
//SamplerState g_sampler : register(s0);

cbuffer cbuff : register(b0)
{
	float3 cameraPosition;
	float3 forward;
	float3 right;
	float3 up;
	float seconds;
};


struct RayPayload
{
    float4 color;
};

[shader("raygeneration")]
void MyRaygenShader()
{
    float2 lerpValues = (float2)DispatchRaysIndex() / (float2)DispatchRaysDimensions();

	float3 centre = cameraPosition + forward;
	float3 screenPos = centre + lerp(-right, right, lerpValues.x) + lerp(up, -up, lerpValues.y);
	//screenPos += seconds / 10.0;

	RayDesc ray;
	ray.Origin = cameraPosition;
    ray.Direction = normalize(screenPos - ray.Origin);
    // Set TMin to a non-zero small value to avoid aliasing issues due to floating - point errors.
    // TMin should be kept small to prevent missing geometry at close contact areas.
    ray.TMin = 0.001;
    ray.TMax = 10000.0;
    RayPayload payload = { float4(0, 0, 0, 0) };
    TraceRay(Scene, RAY_FLAG_CULL_BACK_FACING_TRIANGLES, ~0, 0, 1, 0, ray, payload);

    RenderTarget[DispatchRaysIndex().xy] = payload.color;
}

[shader("closesthit")]
void MyClosestHitShader(inout RayPayload payload, in BuiltInTriangleIntersectionAttributes attr)
{
	float3 lightDirection = normalize(float3(0.3, -1.0, -0.2));

	// Get the normal from the vertex buffer (note odd order of barycentrics)
	uint vertId = 3 * PrimitiveIndex();
	float3 hitNormal = vertices[vertId + 1].normal * attr.barycentrics.x +
		vertices[vertId + 2].normal * attr.barycentrics.y +
		vertices[vertId + 0].normal * (1.0 - attr.barycentrics.x - attr.barycentrics.y);
	// Apply rotation part of world transform to normal to make world-normal
	hitNormal = normalize(mul(hitNormal, (float3x3)ObjectToWorld4x3()));

	// Get the texture coordinates from the vertex buffer
	float2 hitTexCoord = vertices[vertId + 1].texCoord * attr.barycentrics.x +
		vertices[vertId + 2].texCoord * attr.barycentrics.y +
		vertices[vertId + 0].texCoord * (1.0 - attr.barycentrics.x - attr.barycentrics.y);
	// Compute brightness with ambient and specular light
	float brightness = 0.4 + 0.6 * saturate(dot(hitNormal, -lightDirection));

	if (InstanceIndex() == 3 && payload.color.a == 0.0) {
		// Send a reflection ray from object 3
		RayPayload pay2 = { float4(0, 0, 0, 1) };

		RayDesc ray;
		ray.Origin = WorldRayOrigin() + WorldRayDirection() * RayTCurrent();
		ray.Direction = reflect(WorldRayDirection(), hitNormal);
		ray.TMin = 0.001;
		ray.TMax = 10000.0;

		TraceRay(Scene, 0, ~0, /*No cull*/0, 1, 0, ray, pay2);
		payload.color = pay2.color * 0.5;
	} else {
		// Work out the colour of the object
		float3 baseColor = float3(0, 0, 0);
		if (InstanceIndex() == 0) baseColor = g_texture.Load(float3(hitTexCoord * 1023, 0.0)).bgr; // Sample(g_sampler, hitTexCoord).rgb;
		else if (InstanceIndex() == 1) baseColor = float3(0, 1, 0);
		else if (InstanceIndex() == 2) baseColor = float3(0, 0, 1);
		else if (InstanceIndex() == 3) baseColor = float3(1, 1, 0);
		else if (InstanceIndex() == 4) baseColor = float3(0, 1, 1);
		else baseColor = float3(1, 1, 1);
		payload.color = float4(baseColor * brightness, 0);
		//payload.color = float4(hitTexCoord, 0, 0);
	}
}

[shader("miss")]
void MyMissShader(inout RayPayload payload)
{
    payload.color = float4(0.0, 1.0, 0.0, 1.0);
}
