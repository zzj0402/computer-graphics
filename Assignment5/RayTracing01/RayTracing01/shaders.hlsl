struct FragmentInput
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
	float brightness : BRIGHTNESS;
};

cbuffer ConstantBuffer : register(b0)
{
	float4x4 world;
	float4x4 view;
	float4x4 projection;
	float3 offset;
	float seconds;
};

Texture2D g_texture : register(t0);
SamplerState g_sampler : register(s0);


FragmentInput VertexMain(float4 position : POSITION, float3 normal : NORMAL,  float2 uv : TEXCOORD)
{
	float3 LightDirection = float3(-0.4, -1.0, -0.5);
	FragmentInput result;

	result.position = mul(mul(projection, mul(view, world)), position);
	result.uv = uv / 4.0;
	float3 worldNormal = normalize(mul((float3x3)world, normal));
	result.brightness = 0.4 + 0.6 * saturate(dot(-LightDirection, worldNormal));

	return result;
}

float4 FragmentMain(FragmentInput input) : SV_TARGET
{
	float4 tex = g_texture.Sample(g_sampler, input.uv);
	return  tex * input.brightness;
}

